package vn.edu.fpt.uytpvgt60819.ivolunteer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

public class InfoActivity extends AppCompatActivity {
    private TextView activity, name, date, time, location;
    private Button back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        String jsonObj = getIntent().getStringExtra("data");
        Gson gs = new Gson();
        Volunteer volunteer = gs.fromJson(jsonObj, Volunteer.class);


        activity = findViewById(R.id.txtActivity);
        name = findViewById(R.id.txtName);
        date = findViewById(R.id.txtDate);
        time = findViewById(R.id.txtTime);
        location = findViewById(R.id.txtLocation);
        back = findViewById(R.id.btnBack);

        activity.setText(volunteer.activity);
        name.setText(volunteer.name);
        date.setText(volunteer.date);
        time.setText(volunteer.time);
        location.setText(volunteer.location);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(InfoActivity.this, MainActivity.class);
                startActivity(next);
            }
        });
    }
}

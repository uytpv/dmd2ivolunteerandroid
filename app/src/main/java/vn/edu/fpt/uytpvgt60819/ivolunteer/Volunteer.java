package vn.edu.fpt.uytpvgt60819.ivolunteer;

/**
 * Created by traphucvinhuy on 3/3/18.
 */

public class Volunteer {
    public String activity, name, date, time, location;

    public Volunteer(String activity, String name, String date, String time, String location) {
        this.activity = activity;
        this.name = name;
        this.date = date;
        this.time = time;
        this.location = location;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
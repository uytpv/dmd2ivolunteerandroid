package vn.edu.fpt.uytpvgt60819.ivolunteer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private Spinner location;
    private Button btnSave;
    private EditText txtActivity;
    private EditText txtName;
    private static TextView txtDate, txtTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        location = (Spinner) findViewById(R.id.location);
        btnSave = (Button) findViewById(R.id.btnSave);
        txtActivity = findViewById(R.id.txtActivity);
        txtName = findViewById(R.id.txtName);
        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        addOnClickListenerOnTxtDate();
        addOnClickListenerOnTxtTime();
        addListenerOnButton();
    }


    private void addOnClickListenerOnTxtDate() {
        txtDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });
    }

    private void addOnClickListenerOnTxtTime() {
        txtTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "timePicker");
            }
        });
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            txtTime.setText(hourOfDay + ":" + minute);
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @SuppressLint("ResourceType")
        public void onDateSet(DatePicker view, int year, int month, int day) {
            String txtYear = Integer.toString(year);
            String txtMonth = month < 10 ? "0" + Integer.toString(month + 1) : Integer.toString(month + 1);
            String txtDay = day < 10 ? "0" + day : Integer.toString(day);
            txtDate.setText(txtYear + "-" + txtMonth + "-" + txtDay);
        }
    }

    private void addListenerOnButton() {
        btnSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String valActivity = txtActivity.getText().toString();
                String valName = txtName.getText().toString();
                String valDate = txtDate.getText().toString();
                String valTime = txtTime.getText().toString();
                String valLocation = location.getSelectedItem().toString();
                if (validateForm(valActivity, valName, valDate, valTime)) {
                    String message = "";

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                    message += "Activity: " + valActivity + "\n";
                    message += "Volunteer Name: " + valName + "\n";
                    message += "Date: " + valDate + "\n";
                    message += "Time: " + valTime + "\n";
                    message += "Location: " + valLocation + "\n";

                    Volunteer vObj = new Volunteer(valActivity, valName, valDate, valTime, valLocation);
                    Gson gs = new Gson();
                    final String jsonObj = gs.toJson(vObj);

                    builder.setTitle("Volunteer Activity Information")
                            .setMessage(message)
                            .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent next = new Intent(MainActivity.this, InfoActivity.class);
                                    next.putExtra("data", jsonObj);

                                    startActivity(next);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            }).show();

                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.O)
    private boolean validateForm(String valActivity, String valName, String valDate, String valTime) {
        boolean validateActivity, validateName, validateDate, validateTime = false;
        if (valActivity.isEmpty()) {
            showToast("Activity is required");
            return false;
        } else {
            if (valActivity.length() < 3 || valActivity.length() > 20) {
                showToast("Activity must be greater than 3 and no more than 20 characters");
                return false;
            } else {
                validateActivity = true;
            }
        }
        if (valName.isEmpty()) {
            showToast("Name is required");
            return false;
        } else {
            if (valName.length() < 3 || valName.length() > 20) {
                showToast("Volunteer name must be greater than 3 and no more than 20 characters");
                return false;
            } else {
                validateName = true;
            }
        }
        if (valDate.isEmpty()) {
            showToast("Date is required");
            return false;
        } else {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
            String currDate = df.format(c);
            valDate = valDate.replace("-", "");
            if (Integer.parseInt(currDate) >= Integer.parseInt(valDate)) {
                showToast("Chosen date must be in the future");
                return false;
            }
            validateDate = true;
        }
        if (valTime.isEmpty()) {
            showToast("Time is required");
            return false;
        } else {
            validateTime = true;
        }
        return validateActivity && validateDate && validateName && validateTime;
    }

    private void showToast(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }


//    private void addListenerOnSpinnerItemSelection() {
//        location.setOnItemSelectedListener(new CustomOnItemSelectedListener());
//    }
}
